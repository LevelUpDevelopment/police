export default images = [
    {
        name: 'firstFloorMap',
        source: require('./../../assets/images/firstFloor.png'),
    },
    {
        name: 'secondFloorMap',
        source: require('./../../assets/images/secondFloor.png'),
    },
    {
        name: 'iotMap',
        source: require('./../../assets/images/launch_fishers_map.png'),
    },
];

  