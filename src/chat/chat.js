import React from 'react';
import { AsyncStorage } from 'react-native';
const io = require('socket.io-client');
import { GiftedChat } from 'react-native-gifted-chat';
import DeviceInfo from 'react-native-device-info';
import _ from 'lodash';

class Chat extends React.Component {

  constructor(props) {
    super(props);

    this.user = DeviceInfo.getDeviceId();
    this.determineUser = this.determineUser.bind(this);
    this.onReceivedMessage = this.onReceivedMessage.bind(this);
    this.onSend = this.onSend.bind(this);
    this.storeMessages = this.storeMessages.bind(this);

    this.socket = io('https://hackaton.interfaces.ws', {
      transports: ['websocket'],
      path: "/socket.io/socket.io.js"
    });
    this.socket.on('message', this.onReceivedMessage);

    this.socket.on('connect', () => {
      console.log('socket connected');
    });

    this.socket.on('connect_error', (err) => {
      console.log(err);
    });

    this.socket.on('disconnect', () => {
      console.log('Disconnected Socket!');
    });

    this.determineUser();

    this.state = {
      messages: [],
      userId: null
    };
  }

  determineUser() {
    const currentChatRoom = (this.props ? this.props.navigation.state.params.chatId : null);
    console.log(currentChatRoom);
    AsyncStorage.getItem(this.user)
      .then((userId) => {
        // If there isn't a stored userId, then fetch one from the server.
        if (!userId) {
          this.socket.emit('userJoined', [null, currentChatRoom]);
          this.socket.on('userJoined', (userId) => { // eslint-disable-line no-shadow
            AsyncStorage.setItem(this.user, userId);
            this.setState({ userId });
          });
        } else {
          this.socket.emit('userJoined', [userId, currentChatRoom]);
          this.setState({ userId });
        }
      })
      .catch((e) => {
        console.log(e);
      });
  }

  // Event listeners
  onReceivedMessage(messages) {
    this.storeMessages(messages);
  }

  onSend(messages = []) {
    const currentChatRoom = (this.props ? this.props.navigation.state.params.chatId : null);
    this.socket.emit('message', messages[0], currentChatRoom);
    messages[0].chatId=currentChatRoom;
    this.storeMessages(messages);
  }

  // Helper functions
  storeMessages(messages) {
    const currentChatRoom = (this.props ? this.props.navigation.state.params.chatId : null);
    const myOwnMessages = [];
    _.each(messages, function(message) { if (message.chatId === currentChatRoom) myOwnMessages.push(message) } );
    this.setState((previousState) => {
      return {
        messages: GiftedChat.append(previousState.messages, myOwnMessages)
      };
    });
  }

  render() {
    const user = { _id: this.state.userId || -1 };

    return (
      <GiftedChat
        messages={this.state.messages}
        onSend={this.onSend}
        user={user}
      />
    );
  }

}

module.exports = Chat;
