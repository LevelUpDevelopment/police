import React, { Component } from 'react';
import AWS from 'aws-sdk/dist/aws-sdk-react-native';
import PropTypes from 'prop-types';
import {
  Platform,
  StyleSheet,
  ImageBackground,
  Text,
  View,
  PixelRatio,
  Button,
  TouchableOpacity
} from 'react-native';

import Dimensions from 'Dimensions';

import Circle from './../circle';
import Zones from './../../mock/mock-zones';
import ChatCoords from './../../mock/chat';
import Images from './../../assets/images';
import TimerMixin from 'react-timer-mixin';
import OriginalDimensions from './../../mock/original-Dimensions';

import Svg from 'react-native-svg';
import { StackNavigator } from 'react-navigation';
import ChatIcon from '../chatIcon.js';


export default class BackgroundMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      allZones: JSON.parse(JSON.stringify(Zones)),
      originalZones: JSON.parse(JSON.stringify(Zones)),
      //originalDimensions: { width: OriginalDimensions.width, height: OriginalDimensions.height},
      currentDimensions: { width:(Dimensions.get('window').width), height: (Dimensions.get('window').height)},
      timer: null,
      gunShots: [{coreid: 0, data: 0}],
      rooms: []
    };
    props.Images = Images;
    
    props.OriginalDimensions = OriginalDimensions;
     this.setUpIotDevices();
     this.getIotData();


     AWS.config.update({
      region: 'us-east-1',
      secretAccessKey: 'fxkvtcFvN5R+0dA8lh/Ee9uVlUifEw0En5aaL0CI',
      accessKeyId: 'AKIAJVNTUUFNHQH2PTLQ'
    })
    this.sqs = new AWS.SQS();

  
    this.getMessage();
    this.getRoomsData();
  };

  getMessage() {
    this.sqs.receiveMessage({
      QueueUrl: 'https://sqs.us-east-1.amazonaws.com/177496848564/ShotsFired',
      MaxNumberOfMessages: 10,
    },
    (err, data) => {
      if (data.Messages && data.Messages.length){
        this.setState({gunShots: JSON.parse(JSON.stringify(data.Messages))});
        for(i = 0; i < data.Messages.length; i++){
          this.sqs.deleteMessage({
            QueueUrl: 'https://sqs.us-east-1.amazonaws.com/177496848564/ShotsFired',
            ReceiptHandle: data.Messages[i].ReceiptHandle
          }, function(err, data){ 
            if(!err) 
              console.log('deleted'); 
            else 
              console.log(err)});
        }
      } else {
        this.setState({gunShots: [{coreid: 0, data: 0}] })
      }
      this.getMessage();
    })
  }

  static propTypes = {
      allZones: PropTypes.array
  }

  componentDidMount() {
    //let getIotDataTimer = setInterval(this.getIotData, 500);
    let getRoomsTimer = setInterval(this.getRoomsData, 3000);
    this.setState({ getRoomsTimer });
  }

  componentWillUnmount() {
    clearInterval(this.state.timer);
  }

  calculateXCoordinateBasedOnScreenSize = (xPos) => {
      var newWidth  = Dimensions.get('window').width;
      var newX = (xPos * newWidth) / OriginalDimensions[0].width;
      return newX;
  }

  calculateYCoordinateBasedOnScreenSize = (yPos) => {
    var newHeight = Dimensions.get('window').height;
    var newY = (yPos * newHeight) /  OriginalDimensions[0].height;
    return newY;
  }

  setUpIotDevices = () => {
    let deviceHeardNoise = false;
    iotDevices = this.state.allZones.map(function(zone, i) {

      deviceHeardNoise = false;
      if(this.state.gunShots && Array.isArray(this.state.gunShots) 
      && this.state.gunShots.length > 0 && this.state.gunShots[0].coreid != 0)
      {
        let device = this.state.gunShots.filter(device => JSON.parse(device.Body).coreid == zone.coreid);

        if (device && device.length > 0)
        {
          deviceHeardNoise = true;
          // deviceHeardNoise = JSON.parse(deviceHeardNoise[0].Body);
          // deviceHeardNoise = deviceHeardNoise[0].data > 200;
        }
      }

      if(this.state.currentDimensions && OriginalDimensions){
        zone.x = this.calculateXCoordinateBasedOnScreenSize(this.state.originalZones[i].x);
        zone.y = this.calculateYCoordinateBasedOnScreenSize(this.state.originalZones[i].y);
      }

      if(deviceHeardNoise){
      return (
        <View>
          <Circle key={(zone.x - 30).toString()+  " " + (zone.y - 30).toString()} radius={90} x={zone.x - 30 } y={zone.y- 30} noiseLevel={1600}> </Circle>  
          <Circle key={(zone.x - 15).toString()+  " " + (zone.y - 15).toString()} radius={60} x={zone.x - 15 } y={zone.y - 15} noiseLevel={1600}> </Circle>
          <Circle key={zone.x.toString()+  "noise " + (zone.y).toString()} radius={30} x={this.state.originalZones[i].x} y={zone.y} noiseLevel={1600}> </Circle>  
        </View>
      );
      }
      else {
        return  <Circle key={zone.x.toString()+  " " + zone.y.toString()} radius={30} x={zone.x} y={zone.y}> </Circle>
      }

    }.bind(this));

    return iotDevices;
  }

  getFloorMap = (mapName) => {
    let imgSource = Images.filter(map => map.name == mapName);
    return imgSource[0].source;
  }

  getIotData = () => {
    return fetch('https://httpbin.org/ip')
      .then((response) => response.json())
      .then((responseJson) => {
      //  this.setState({gunShots: responseJson});
        return responseJson;
      })
      .catch((error) => {
        console.error(error);
        return null;
      });
   }

  getRoomsData = () => {
    return fetch('http://iotcivic.us-east-1.elasticbeanstalk.com/rooms/all')
      .then((response) => response.json())
      .then((responseJson) => {
        //console.log(responseJson);
        return this.setState({ rooms: responseJson });
      })
      .catch((error) => {
        console.error(error);
        return null;
      });
   }

  showChatIcons() {
    return this.state.rooms.map((r) => {
      if (r.isSafe !== null) {
        return (
          <ChatIcon key={r.x.toString()+  " " + r.y.toString()} navigation={this.props.navigation} id={r.id} x={r.x} y={r.y} isSafe={r.isSafe} />
        );
      }
      return null;
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={this.getFloorMap(this.props.navigation.state.params.mapName)}
        imageStyle={styles.image}
        style={styles.image}>
          {this.setUpIotDevices()} 
          {this.showChatIcons()} 
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'relative',
    backgroundColor: '#F5FCFF',
  },
  image:{
      resizeMode: 'stretch',
      flex:1,
      width: '100%',
  },
  circleContainer:{
    position: 'absolute',
    top: 72,
    left: 33
},
    circle: {
        height: 30,
        width: 30,
        borderRadius: 30,
        backgroundColor: '#333'
    }
});
