import React, { Component } from 'react';
import {
  Image,
  View,
  TouchableOpacity
} from 'react-native';

export default class ChatIcon extends Component {

  constructor(props) {
    super(props);
    this.state = {
      x: -30, y: -30, isSafe: null
    };
  }

  componentDidMount() {
    this.setState({
      id: this.props.id,
      x: this.props.x,
      y: this.props.y,
      isSafe: this.props.isSafe
    });
  }

  componentWillReceiveProps(nextProps) {
    let setState = false;
    if (this.state.isSafe !== nextProps.isSafe) {
      setState = true;
    }
    if (this.state.x !== nextProps.isSafe) {
      setState = true;
    }
    if (this.state.y !== nextProps.isSafe) {
      setState = true;
    }
    this.setState({ isSafe: nextProps.isSafe, x: nextProps.x, y: nextProps.y });
  }

  openChat(id) {
    this.props.navigation.navigate('Chat', {
      chatId: id,
    });
  }

  getChatIconImage() {
    if (this.state.isSafe) {
      return (
        <TouchableOpacity onPress={this.openChat.bind(this, this.state.id)}>
          <Image source={require('../assets/images/chatGreen.png')} style={{ width: 55, height: 55 }} />
        </TouchableOpacity>
      );
    }
    return (
      <TouchableOpacity onPress={this.openChat.bind(this, this.state.id)}>
        <Image source={require('../assets/images/chatRed.png')} style={{ width: 55, height: 55 }} />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={{ position: 'absolute', left: this.state.x, top: this.state.y }}>
        {this.getChatIconImage()}
      </View>
    );
  }

}
