import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  ImageBackground,
  Text,
  View,
  PixelRatio
} from 'react-native';


export default class Circle extends Component {

  constructor(props) {
    super(props);
    this.state = {
      x: -30,
      y: -30,
      radius: 30,
      noiseLevel: 200
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(prevState.radius !== nextProps.radius){
      return{
        radius: nextProps.radius
      }
    }
    return null;
  }

  componentDidMount() {
    this.setState({
      x: this.props.x, 
      y: this.props.y, 
      radius: this.props.radius ? this.props.radius : this.state.radius, 
      noiseLevel: this.props.noiseLevel ? this.props.noiseLevel : this.state.radius
    });
  }

  getBackgroundColor(noiseLevel) {
    if(noiseLevel >= 1600) {
      return 'rgba(255,0,0,0.4)';
    } else {
      return 'rgba(255,255,0,0.7)';
    }
  }

  render() {
    return (
      <View style={{ position: 'absolute', left: this.state.x, top: this.state.y }}>
        <View style={{ height: this.state.radius, width: this.state.radius, 
          borderRadius: this.state.radius / PixelRatio.get(), 
          backgroundColor: this.getBackgroundColor(this.state.noiseLevel)
        }}>
        </View>
      </View>
    );
  }
}