import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

import BackgroundMap from './src/backgroundImage/backgroundMap';
import Chat from './src/chat/chat';
import { StackNavigator } from 'react-navigation';

console.disableYellowBox = true;

class HomeScreen extends React.Component {
  render() {
    return (
        <View style={styles.container}>
         <Text style={styles.welcome}>
          First Responder App
        </Text>
        <Button
          title="View 1st floor"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('BackgroundMap', {
              mapName: 'iotMap',
            });
          }}
        />
        <Button
          title="View 2nd floor"
          onPress={() => {
            /* 1. Navigate to the Details route with params */
            this.props.navigation.navigate('BackgroundMap', {
              mapName: 'secondFloorMap',
            });
          }}
        />
      </View>
    );
  }
}

export default class App extends Component {
  render() {
    return <RootStack />;
  }
}

class ChatModalScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text style={{ fontSize: 30 }}>This is a Chat Room!</Text>
        <Button

          onPress={() => this.props.navigation.goBack()}
          title="Dismiss"
        />
      </View>
    );
  }
}

const MainStack = StackNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: "Shots Fired"
      }
    },
    BackgroundMap: {
      screen: BackgroundMap,
      navigationOptions: {
        title: "Shots Fired"
      }
    },
    Chat: {
      screen: Chat,
      navigationOptions: {
        title: "Victim Chat"
      }
    },
  },
  {
    initialRouteName: 'Home',
  }
);

const RootStack = StackNavigator(
  {
    Main: {
      screen: MainStack,
    },
    ChatModal: {
      screen: ChatModalScreen,
    }
  },
  {
    mode: 'modal',
    headerMode: 'none',
    title: "Shots Fired"
  }
);


const styles = StyleSheet.create({
  container: {
    flex: 1,
   // justifyContent: 'center',
   // alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
