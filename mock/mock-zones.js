export default [
    {
        key: 1,
        x: 113 -80,
        y: 160- 60,
        noiseLevel: 1623,
        checkedIn: false
    },
    {
        key: 2,
        coreid: '220052000c51343334363138',
        x: 193 -80,
        y: 132- 60,
        noiseLevel: 1523
    },
    {
        key: 3,
        coreid: '220052000c51343334363138',
        x: 322 -80,
        y: 132- 60,
        noiseLevel: 1523
    },
    {
        key: 4,
        coreid: '1d0051001951343334363036',
        x: 452-80,
        y: 132- 60,
        noiseLevel: 1423
    },
    {
        key: 5,
        coreid: '1d0051001951343334363036',
        x: 578-80,
        y: 132- 60,
        noiseLevel: 1423
    },
    {
        key: 6,
        x: 705-80,
        y: 132- 60,
        noiseLevel: 1423
    },
    {
        key: 7,
        x: 834-80,
        y: 132- 60,
        noiseLevel: 1423
    },
    {
        key: 8,
        x: 958-80,
        y: 132- 60,
        noiseLevel: 1423
    },
    {
        key: 9,
        x: 705-80,
        y: 365- 60,
        noiseLevel: 1423
    },
    {
        key: 10,
        x: 834-80,
        y: 365- 60,
        noiseLevel: 1423
    },
    {
        key: 11,
        x: 958-80,
        y: 365- 60,
        noiseLevel: 1023
    },
    {
        key: 12,
        x: 95-80,
        y: 365- 60,
        noiseLevel: 1623
    },
    {
        key: 13,
        x: 95-80,
        y: 495- 60,
        noiseLevel: 1023
    },
    {
        key: 14,
        x: 216-80,
        y: 606- 60,
        noiseLevel: 1023
    },
    {
        key: 15,
        x: 260-80,
        y: 257- 60,
        noiseLevel: 1023
    },
    {
        key: 16,
        x: 481-80,
        y: 257- 60,
        noiseLevel: 1023
    },
    {
        key: 17,
        x: 921-80,
        y: 257- 60,
        noiseLevel: 1023
    },
    {
        key: 18,
        x: 681-80,
        y: 257- 60,
        noiseLevel: 1023
    },
    {
        key: 19,
        x: 212-80,
        y: 434- 60,
        noiseLevel: 1023
    },
    {
        key: 20,
        x: 681-80,
        y: 606- 60,
        noiseLevel: 1023
    },
    {
        key: 21,
        x: 452-80,
        y: 533- 60,
        noiseLevel: 1023
    },
    {
        key: 22,
        x: 958-80,
        y: 606- 60,
        noiseLevel: 1023
    },
];