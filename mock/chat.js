export default [
  {
    id: 1,
    x: 958-80,
    y: 130,
    isSafe: true
  },
  {
    id: 2,
    x: 330,
    y: 189,
    isSafe: true
  },
  {
    id: 3,
    x: 708,
    y: 30,
    isSafe: false
  }
];